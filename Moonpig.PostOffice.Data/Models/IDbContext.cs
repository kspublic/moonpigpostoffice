﻿namespace Moonpig.PostOffice.Data.Models
{
    public interface IDbContext
    {
        int? GetProductLeadTimeDays(int productId);
    }
}
