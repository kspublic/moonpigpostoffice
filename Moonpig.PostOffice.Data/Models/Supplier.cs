﻿namespace Moonpig.PostOffice.Data.Models
{
    public class Supplier
    {
        public int SupplierId { get; set; }
        public string Name { get; set; }
        public int LeadTime { get; set; }
    }
}
