﻿using System;
using System.Collections.Generic;
using Moonpig.PostOffice.Api.Controllers;
using Moonpig.PostOffice.Api.Model;
using Moonpig.PostOffice.Api.Services;
using Moonpig.PostOffice.Data;
using Moonpig.PostOffice.Data.Models;
using Shouldly;
using Xunit;

namespace Moonpig.PostOffice.Tests
{
    public class PostOfficeTests
    {
        private readonly IDespatchProcessor _despatchProcessor;

        public PostOfficeTests()
        {
            //As for the whole exercise we are using "mock" data, I will use the same one for tests
            //In normal life scenario we would have another implementation of the IDbContext just for tests
            //(taken from different source etc.)
            IDbContext dbContext = new DbContext();
            _despatchProcessor = new DespatchProcessor(dbContext);
        }

        [Fact]
        public void OneProductWithLeadTimeOfOneDay()
        {
            var controller = new DespatchDateController(_despatchProcessor);
            var date = controller.Get(new List<int>() {1}, DateTime.Now);
            date.Date.Date.ShouldBe(DateTime.Now.Date.AddDays(1));
        }

        [Fact]
        public void OneProductWithLeadTimeOfTwoDay()
        {
            var controller = new DespatchDateController(_despatchProcessor);
            var date = controller.Get(new List<int>() { 2 }, DateTime.Now);
            date.Date.Date.ShouldBe(DateTime.Now.Date.AddDays(2));
        }

        [Fact]
        public void OneProductWithLeadTimeOfThreeDay()
        {
            var controller = new DespatchDateController(_despatchProcessor);
            var date = controller.Get(new List<int>() { 3 }, DateTime.Now);
            date.Date.Date.ShouldBe(DateTime.Now.Date.AddDays(3));
        }

        [Fact]
        public void SaturdayHasExtraTwoDays()
        {
            var controller = new DespatchDateController(_despatchProcessor);
            var date = controller.Get(new List<int>() { 1 }, new DateTime(2018,1,26));
            date.Date.ShouldBe(new DateTime(2018, 1, 26).Date.AddDays(3));
        }

        [Fact]
        public void SundayHasExtraDay()
        {
            var controller = new DespatchDateController(_despatchProcessor);
            var date = controller.Get(new List<int>() { 3 }, new DateTime(2018, 1, 25));

            //As the new despatch processor takes into account weekends - we need to skip them in calc
            //Corrected this test to represent new despatch processor
            date.Date.ShouldBe(new DateTime(2018, 1, 30));
        }
    }
}
