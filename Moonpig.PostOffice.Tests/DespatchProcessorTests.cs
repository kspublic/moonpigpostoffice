﻿using System;
using System.Collections.Generic;
using Moonpig.PostOffice.Api.Model;
using Moonpig.PostOffice.Api.Services;
using Moonpig.PostOffice.Data.Models;
using Moq;
using Shouldly;
using Xunit;

namespace Moonpig.PostOffice.Tests
{
    public class DespatchProcessorTests
    {
        private readonly IDespatchProcessor _despatchProcessor;
        private readonly Mock<IDbContext> _dbContext;

        public DespatchProcessorTests()
        {
            _dbContext = new Mock<IDbContext>();
            _despatchProcessor = new DespatchProcessor(_dbContext.Object);
        }
        
        //Additional tests to cover all "Acceptance Criteria" from readme.md:

        [Theory]
        //Lead time added to despatch date
        [InlineData(1, 2018, 1, 1, 2018, 1, 2)]
        [InlineData(2, 2018, 1, 1, 2018, 1, 3)]
        //Lead time is not counted over a weekend
        [InlineData(1, 2018, 1, 5, 2018, 1, 8)]
        [InlineData(1, 2018, 1, 6, 2018, 1, 9)]
        [InlineData(1, 2018, 1, 7, 2018, 1, 9)]
        //Lead time over multiple weeks
        [InlineData(6, 2018, 1, 5, 2018, 1, 15)]
        [InlineData(11, 2018, 1, 5, 2018, 1, 22)]
        public void GivenLeadTimesAndOrderDateWhenDespatchProcessorCalculatesThenTheDespatchDateIs(
            int maxLeadTime, int orderY, int orderM, int orderD, int expectedY, int expectedM, int expectedD)
        {
            var actualDate = _despatchProcessor.Calculate(maxLeadTime, new DateTime(orderY, orderM, orderD));
            var expectedDate = new DateTime(expectedY, expectedM, expectedD);
            actualDate.Date.ShouldBe(expectedDate);
        }
        
        //Supplier with longest lead time is used for calculation
        [Fact]
        public void GivenProductIdsWhenDespatchProcessorGetsMaxLeadTimeThenReturnsLongestLeadTimeValue()
        {
            _dbContext.Setup(x => x.GetProductLeadTimeDays(1)).Returns(1);
            _dbContext.Setup(x => x.GetProductLeadTimeDays(2)).Returns(2);
            var actualLeadTime = _despatchProcessor.GetMaxLeadTime(new List<int>(){1,2});
            actualLeadTime.ShouldBe(2);
        }

        [Fact]
        public void GivenProductIdsWhenDespatchProcessorGetsMaxLeadTimeThenReturnsLongestLeadTimeValue2()
        {
            _dbContext.Setup(x => x.GetProductLeadTimeDays(1)).Returns(5);
            _dbContext.Setup(x => x.GetProductLeadTimeDays(2)).Returns(3);
            _dbContext.Setup(x => x.GetProductLeadTimeDays(3)).Returns(11);
            _dbContext.Setup(x => x.GetProductLeadTimeDays(4)).Returns(6);
            var actualLeadTime = _despatchProcessor.GetMaxLeadTime(new List<int>() { 1, 2, 3, 4 });
            actualLeadTime.ShouldBe(11);
        }
    }
}
