﻿using System;
using System.Collections.Generic;
using Moonpig.PostOffice.Api.Model;
using Moonpig.PostOffice.Data.Models;

namespace Moonpig.PostOffice.Api.Services
{
    public class DespatchProcessor : IDespatchProcessor
    {
        private readonly IDbContext _dbContext;

        public DespatchProcessor(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int GetMaxLeadTime(IEnumerable<int> productIds)
        {
            var maxLeadTime = 0;

            foreach (var productId in productIds)
            {
                var productLeadTime = _dbContext.GetProductLeadTimeDays(productId) ?? 0;
                if (productLeadTime > maxLeadTime) maxLeadTime = productLeadTime;
            }

            return maxLeadTime;
        }

        public DespatchDate Calculate(int maxLeadTime, DateTime orderDate)
        {
            //Add additional lead time if the order was places over the weekend
            maxLeadTime += IsWeekend(orderDate) ? 1 : 0;

            var maxLeadTimeDate = orderDate;
            while (maxLeadTime > 0)
            {
                maxLeadTimeDate = maxLeadTimeDate.AddDays(1);
                if (IsWeekend(maxLeadTimeDate)) continue;
                maxLeadTime--;
            }

            return new DespatchDate() { Date = maxLeadTimeDate };
        }

        private static bool IsWeekend(DateTime dateTime)
        {
            return dateTime.DayOfWeek == DayOfWeek.Saturday || dateTime.DayOfWeek == DayOfWeek.Sunday;
        }
    }
}