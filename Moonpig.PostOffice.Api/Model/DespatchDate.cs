﻿using System;

namespace Moonpig.PostOffice.Api.Model
{
    public class DespatchDate
    {
        public DateTime Date { get; set; }
    }
}