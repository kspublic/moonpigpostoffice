﻿using System;
using System.Collections.Generic;

namespace Moonpig.PostOffice.Api.Model
{
    public interface IDespatchProcessor
    {
        int GetMaxLeadTime(IEnumerable<int> productIds);
        DespatchDate Calculate(int maxLeadTime, DateTime orderDate);
    }
}