﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Moonpig.PostOffice.Api.Model;

namespace Moonpig.PostOffice.Api.Controllers
{
    [Route("api/[controller]")]
    public class DespatchDateController : Controller
    {
        private readonly IDespatchProcessor _despatchProcessor;

        public DespatchDateController(IDespatchProcessor despatchProcessor)
        {
            _despatchProcessor = despatchProcessor;
        }
        
        [HttpGet]
        public DespatchDate Get(List<int> productIds, DateTime orderDate)
        {
            var maxLeadTime = _despatchProcessor.GetMaxLeadTime(productIds);
            return _despatchProcessor.Calculate(maxLeadTime, orderDate);
        }
    }
}
